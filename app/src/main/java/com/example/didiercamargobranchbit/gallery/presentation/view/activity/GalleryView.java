package com.example.didiercamargobranchbit.gallery.presentation.view.activity;

import com.example.didiercamargobranchbit.app.base.BaseView;

public interface GalleryView extends BaseView {
    void selectImagesFromGallery();
    void takePictureFromCamera();
    void showMessagePictureSavedSuccessfully();
    void showErrorMessage();
}
