package com.example.didiercamargobranchbit.gallery.presentation.presenter;

import android.net.Uri;

import com.example.didiercamargobranchbit.gallery.model.GalleryModel;
import com.example.didiercamargobranchbit.gallery.presentation.view.activity.GalleryView;

import java.util.List;

public class GalleryPresenter {

    private GalleryView galleryView;
    private GalleryModel galleryModel;

    public GalleryPresenter(GalleryView galleryView){
        this.galleryView = galleryView;
        this.galleryModel = new GalleryModel();
    }

    public void executeSavePicturesFirebase(List<Uri> images){
        galleryModel.setSavePicturesFirebaseListener(new GalleryModel.SavePicturesFirebaseListener() {
            @Override
            public void onSuccess() {
                galleryView.showMessagePictureSavedSuccessfully();
            }

            @Override
            public void onError(String errorMessage) {
                galleryView.showErrorMessage();
            }

            @Override
            public void onFailure(String failureMessage) {
                galleryView.showErrorMessage();
            }
        });
        galleryModel.executeSavePicturesFirebase(images);
    }

}
