package com.example.didiercamargobranchbit.gallery.model;

import android.net.Uri;

import com.example.didiercamargobranchbit.app.firebase.FirebaseManager;
import com.example.didiercamargobranchbit.gallery.interactor.SavePicturesFirebaseInteractor;

import java.util.List;

public class GalleryModel implements
        SavePicturesFirebaseInteractor.SavePicturesFirebaseListener {

    private SavePicturesFirebaseListener savePicturesFirebaseListener;

    public interface SavePicturesFirebaseListener {
        void onSuccess();

        void onError(String errorMessage);

        void onFailure(String failureMessage);
    }

    public void setSavePicturesFirebaseListener(SavePicturesFirebaseListener savePicturesFirebaseListener) {
        this.savePicturesFirebaseListener = savePicturesFirebaseListener;
    }

    public void executeSavePicturesFirebase(List<Uri> images) {
        FirebaseManager.savePictures(images, this);
    }

    @Override
    public void onSuccessSavePicturesFirebase() {
        savePicturesFirebaseListener.onSuccess();
    }

    @Override
    public void onErrorSavePicturesFirebase(String errorMessage) {

    }

    @Override
    public void onFailureSavePicturesFirebase(String failureMessage) {

    }
}
