package com.example.didiercamargobranchbit.gallery.presentation.view.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.didiercamargobranchbit.R;
import com.example.didiercamargobranchbit.app.base.BaseActivity;
import com.example.didiercamargobranchbit.databinding.ActivityGalleryBinding;
import com.example.didiercamargobranchbit.gallery.presentation.presenter.GalleryPresenter;
import com.example.didiercamargobranchbit.gallery.presentation.view.adapter.GalleryAdapter;

import java.util.ArrayList;

public class GalleryActivity extends BaseActivity implements GalleryView{

    private static final String TAG = GalleryActivity.class.getSimpleName();
    private static final int PICK_IMAGE_MULTIPLE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private int position = 1;
    private GalleryAdapter mAdapter;
    private ActivityGalleryBinding binding;
    private GalleryPresenter presenter;
    private Resources res;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery);
        binding.setSelectOption(new SelectOption());
        res = getResources();
        setAdapter();
        presenter = new GalleryPresenter(this);
    }

    @Override
    public void selectImagesFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_MULTIPLE);
    }

    @Override
    public void takePictureFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    public class SelectOption {

        public void onClickSelectImagesFromGallery(View view){
            selectImagesFromGallery();
        }

        public void onClickTakePictureFromCamera(View view){
            takePictureFromCamera();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data) {
            ArrayList<Uri> mArrayUri = new ArrayList<>();
            if (data.getClipData() != null) {
                int cout = data.getClipData().getItemCount();
                Log.d(TAG, "Number images selected: " + cout);
                binding.rvMultipleSelectedImages.setVisibility(View.VISIBLE);
                binding.ivSelectedPicture.setVisibility(View.INVISIBLE);
                for (int i = 0; i < cout; i++) {
                    Uri imageurl = data.getClipData().getItemAt(i).getUri();
                    mArrayUri.add(imageurl);
                }
                mAdapter.setArrayUri(mArrayUri);

                Toast.makeText(this, "Almacenando imágenes en Firebase...", Toast.LENGTH_LONG).show();

                presenter.executeSavePicturesFirebase(mArrayUri);

                position = 0;
            } else {
                Uri imageurl = data.getData();
                binding.ivSelectedPicture.setVisibility(View.VISIBLE);
                binding.rvMultipleSelectedImages.setVisibility(View.INVISIBLE);
                binding.ivSelectedPicture.setImageURI(imageurl);
                mArrayUri.add(imageurl);

                Toast.makeText(this, "Almacenando imagen en Firebase...", Toast.LENGTH_LONG).show();

                presenter.executeSavePicturesFirebase(mArrayUri);

                position = 0;
            }
        } else if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            binding.ivSelectedPicture.setImageBitmap(imageBitmap);
        }
    }

    @Override
    public void showMessagePictureSavedSuccessfully() {
        Toast.makeText(this, "¡Almacenamiento exitoso!", Toast.LENGTH_LONG).show();
    }

    private void setAdapter(){
        mAdapter = new GalleryAdapter(this);
        binding.rvMultipleSelectedImages.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        binding.rvMultipleSelectedImages.setAdapter(mAdapter);
    }

}
