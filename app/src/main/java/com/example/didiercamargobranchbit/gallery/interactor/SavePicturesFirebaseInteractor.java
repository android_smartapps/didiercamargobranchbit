package com.example.didiercamargobranchbit.gallery.interactor;

public interface SavePicturesFirebaseInteractor {

    interface SavePicturesFirebaseListener{
        void onSuccessSavePicturesFirebase();
        void onErrorSavePicturesFirebase(String errorMessage);
        void onFailureSavePicturesFirebase(String errorMessage);
    }

}
