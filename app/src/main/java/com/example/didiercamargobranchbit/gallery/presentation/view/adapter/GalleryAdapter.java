package com.example.didiercamargobranchbit.gallery.presentation.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.didiercamargobranchbit.R;
import com.example.didiercamargobranchbit.app.generic.BindingViewHolder;
import com.example.didiercamargobranchbit.databinding.ItemSelectedImageBinding;

import java.util.ArrayList;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.BindingViewHolderGallery> {

    private LayoutInflater mLayoutInflater;
    private ArrayList<Uri> mArrayUri;
    private Activity activity;

    public GalleryAdapter(Activity activity){
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mArrayUri = new ArrayList<>();
    }

    public void setArrayUri(ArrayList<Uri> mArrayUri) {
        this.mArrayUri = mArrayUri;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BindingViewHolderGallery onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSelectedImageBinding binding;
        binding = DataBindingUtil.inflate(mLayoutInflater, R.layout.item_selected_image, parent, false);
        return new BindingViewHolderGallery(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolderGallery holder, int position) {
        ItemSelectedImageBinding binding = holder.getBinding();
        Uri uri = mArrayUri.get(position);
        binding.ivSelectedImage.setImageURI(uri);
    }

    @Override
    public int getItemCount() {
        return mArrayUri.size();
    }

    public static class BindingViewHolderGallery extends BindingViewHolder<ItemSelectedImageBinding>{

        public BindingViewHolderGallery(ItemSelectedImageBinding view){
            super(view);
        }

    }

}
