package com.example.didiercamargobranchbit.feed.presentation.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.didiercamargobranchbit.R;
import com.example.didiercamargobranchbit.app.generic.BindingViewHolder;
import com.example.didiercamargobranchbit.databinding.ItemFeedBinding;
import com.example.didiercamargobranchbit.feed.data.Movie;

import java.util.ArrayList;
import java.util.List;

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.BindingViewHolderFeed> {

    private static final String URL_MOVIE_IMAGES = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
    private LayoutInflater mLayoutInflater;
    private List<Movie> popularMovies;
    private Activity activity;
    private Resources res;
    private FeedItemClickListener feedItemClickListener;

    public interface FeedItemClickListener{
        void onClickFeedItem(Movie movie);
    }
    public FeedAdapter(Activity activity){
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        popularMovies = new ArrayList<>();
        res = activity.getResources();
    }

    public void setPopularMovies(List<Movie> popularMovies) {
        this.popularMovies = popularMovies;
        notifyDataSetChanged();
    }

    public void setFeedItemClickListener(FeedItemClickListener feedItemClickListener) {
        this.feedItemClickListener = feedItemClickListener;
    }

    @NonNull
    @Override
    public BindingViewHolderFeed onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFeedBinding binding;
        binding = DataBindingUtil.inflate(mLayoutInflater, R.layout.item_feed, parent, false);
        return new BindingViewHolderFeed(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BindingViewHolderFeed holder, int position) {
        ItemFeedBinding binding = holder.getBinding();
        Movie principalMovie = popularMovies.get(position);
        Glide.with(activity)
                .load(String.format("%s%s", URL_MOVIE_IMAGES, principalMovie.getBackdropPath()))
                .into(binding.ivPictureMovie);
        binding.tvTitle.setText(principalMovie.getTitle());
        binding.tvShortDescription.setText(principalMovie.getOverview());
        binding.cvFeed.setOnClickListener(view -> {
            feedItemClickListener.onClickFeedItem(principalMovie);
        });
    }

    @Override
    public int getItemCount() {
        return popularMovies.size();
    }

    public static class BindingViewHolderFeed extends BindingViewHolder<ItemFeedBinding>{

        public BindingViewHolderFeed(ItemFeedBinding view){
            super(view);
        }

    }

}
