package com.example.didiercamargobranchbit.feed.presentation.presenter;

import com.example.didiercamargobranchbit.feed.data.Movie;
import com.example.didiercamargobranchbit.feed.model.GetFeedModel;
import com.example.didiercamargobranchbit.feed.presentation.view.activity.FeedView;

import java.util.List;

public class FeedPresenter {

    private FeedView feedView;
    private GetFeedModel feedModel;

    public FeedPresenter(FeedView feedView){
        this.feedView = feedView;
        this.feedModel = new GetFeedModel();
    }

    public void executeGetPopularMovies(){
        feedModel.executeGetPopularMovies();
        feedModel.setFeedListener(new GetFeedModel.FeedListener() {
            @Override
            public void onSuccessFeed(List<Movie> popularMovies) {
                feedView.showPopularMovies(popularMovies);
            }

            @Override
            public void onErrorFeed(String errorMessage) {
                feedView.showErrorMessage();
            }

            @Override
            public void onFailureFeed(String failureMessage) {
                feedView.showErrorMessage();
            }
        });
    }

    public void executeSavePopularMovies(List<Movie> movies){
        feedModel.executeSavePopularMovies(movies);
        feedModel.setSaveFeedListener(new GetFeedModel.SaveFeedListener() {
            @Override
            public void onSuccessSaveFeed() {

            }

            @Override
            public void onErrorSaveFeed(String errorMessage) {

            }

            @Override
            public void onFailureSaveFeed(String failureMessage) {

            }
        });
    }

    public void executeGetPopularMoviesOffline(){
        feedModel.setGetFeedOfflineListener(new GetFeedModel.GetFeedOfflineListener() {
            @Override
            public void onSuccessGetFeedOffline(List<Movie> popularMovies) {
                feedView.showPopularMovies(popularMovies);
            }

            @Override
            public void onErrorGetFeedOffline(String errorMessage) {
                feedView.showErrorMessage();
            }

            @Override
            public void onFailureGetFeedOffline(String failureMessage) {
                feedView.showErrorMessage();
            }
        });
        feedModel.executeGetPopularMoviesOffline();
    }

}
