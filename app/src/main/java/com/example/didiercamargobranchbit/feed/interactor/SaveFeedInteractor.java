package com.example.didiercamargobranchbit.feed.interactor;

public interface SaveFeedInteractor {

    interface SaveFeedListener {
        void onSuccessSaveFeed();

        void onErrorSaveFeed(String errorMessage);

        void onFailureSaveFeed(String failureMessage);
    }

}
