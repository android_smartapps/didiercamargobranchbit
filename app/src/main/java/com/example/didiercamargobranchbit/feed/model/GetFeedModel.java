package com.example.didiercamargobranchbit.feed.model;

import com.example.didiercamargobranchbit.app.retrofit.CallServicesRetrofit;
import com.example.didiercamargobranchbit.app.sqlite.SQLManager;
import com.example.didiercamargobranchbit.feed.data.Movie;
import com.example.didiercamargobranchbit.feed.interactor.FeedInteractor;
import com.example.didiercamargobranchbit.feed.interactor.FeedOfflineInteractor;
import com.example.didiercamargobranchbit.feed.interactor.SaveFeedInteractor;

import java.util.List;

public class GetFeedModel implements FeedInteractor.FeedListener,
        FeedOfflineInteractor.GetFeedOfflineListener,
        SaveFeedInteractor.SaveFeedListener {

    private static final String TAG = GetFeedModel.class.getSimpleName();

    private FeedListener feedListener;
    private GetFeedOfflineListener getFeedOfflineListener;
    private SaveFeedListener saveFeedListener;

    public interface FeedListener {
        void onSuccessFeed(List<Movie> popularMovies);

        void onErrorFeed(String errorMessage);

        void onFailureFeed(String failureMessage);
    }

    public interface GetFeedOfflineListener {
        void onSuccessGetFeedOffline(List<Movie> popularMovies);

        void onErrorGetFeedOffline(String errorMessage);

        void onFailureGetFeedOffline(String failureMessage);
    }

    public interface SaveFeedListener {
        void onSuccessSaveFeed();

        void onErrorSaveFeed(String errorMessage);

        void onFailureSaveFeed(String failureMessage);
    }

    public void setFeedListener(FeedListener feedListener) {
        this.feedListener = feedListener;
    }

    public void setSaveFeedListener(SaveFeedListener saveFeedListener) {
        this.saveFeedListener = saveFeedListener;
    }

    public void setGetFeedOfflineListener(GetFeedOfflineListener getFeedOfflineListener) {
        this.getFeedOfflineListener = getFeedOfflineListener;
    }

    public void executeGetPopularMovies() {
        CallServicesRetrofit.executeGetPopularMovies(this);
    }

    public void executeSavePopularMovies(List<Movie> movies) {
        for (Movie movie : movies) {
            SQLManager.saveMovie(movie, this);
        }
    }

    public void executeGetPopularMoviesOffline() {
        SQLManager.getPopularMovies(this);
    }

    @Override
    public void onSuccessFeed(List<Movie> popularMovies) {
        feedListener.onSuccessFeed(popularMovies);
    }

    @Override
    public void onErrorFeed(String errorMessage) {

    }

    @Override
    public void onFailureFeed(String failureMessage) {

    }

    @Override
    public void onSuccessGetFeedOffline(List<Movie> popularMovies) {
        getFeedOfflineListener.onSuccessGetFeedOffline(popularMovies);
    }

    @Override
    public void onErrorGetFeedOffline(String errorMessage) {

    }

    @Override
    public void onFailureGetFeedOffline(String failureMessage) {

    }

    @Override
    public void onSuccessSaveFeed() {
        saveFeedListener.onSuccessSaveFeed();
    }

    @Override
    public void onErrorSaveFeed(String errorMessage) {

    }

    @Override
    public void onFailureSaveFeed(String failureMessage) {

    }
}
