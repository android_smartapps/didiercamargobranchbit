package com.example.didiercamargobranchbit.feed.presentation.view.activity;

import com.example.didiercamargobranchbit.app.base.BaseView;
import com.example.didiercamargobranchbit.feed.data.Movie;

import java.util.List;

public interface FeedView extends BaseView {
    void showPopularMovies(List<Movie> popularMovies);
    void goToFeedDetailActivity(Movie movie);
}
