package com.example.didiercamargobranchbit.feed.presentation.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.didiercamargobranchbit.R;
import com.example.didiercamargobranchbit.app.base.BaseActivity;
import com.example.didiercamargobranchbit.databinding.ActivityFeedBinding;
import com.example.didiercamargobranchbit.feed.data.Movie;
import com.example.didiercamargobranchbit.feed.presentation.presenter.FeedPresenter;
import com.example.didiercamargobranchbit.feed.presentation.view.adapter.FeedAdapter;
import com.example.didiercamargobranchbit.gallery.presentation.view.activity.GalleryActivity;
import com.example.didiercamargobranchbit.map.presentation.view.MapActivity;
import com.google.android.material.navigation.NavigationView;

import java.util.List;

public class FeedActivity extends BaseActivity implements
        FeedView, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = FeedActivity.class.getSimpleName();
    private ActivityFeedBinding binding;
    private FeedAdapter adapter;
    private FeedPresenter feedPresenter;
    private Resources res;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feed);
        setAdapter();
        feedPresenter = new FeedPresenter(this);
        res = getResources();

        if (isConnectedToInternet(this)) {
            feedPresenter.executeGetPopularMovies();
        } else {
            feedPresenter.executeGetPopularMoviesOffline();
            Toast.makeText(this, res.getString(R.string.not_internet_connection), Toast.LENGTH_LONG).show();
        }

        setSupportActionBar(binding.toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, binding.drawerLayout, binding.toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.setDrawerListener(toggle);
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_feed) {

        } else if (id == R.id.nav_map) {
            startActivity(new Intent(this, MapActivity.class));
        } else if (id == R.id.nav_gallery) {
            startActivity(new Intent(this, GalleryActivity.class));
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void showPopularMovies(List<Movie> popularMovies) {
        adapter.setPopularMovies(popularMovies);
        if(isConnectedToInternet(this))
        feedPresenter.executeSavePopularMovies(popularMovies);
    }

    @Override
    public void goToFeedDetailActivity(Movie movie) {
        Intent intent = new Intent(this, FeedDetailActivity.class);
        intent.putExtra(FeedDetailActivity.EXTRA_MOVIE, movie);
        startActivity(intent);
    }

    private void setAdapter(){
        adapter = new FeedAdapter(this);
        adapter.setFeedItemClickListener(this::goToFeedDetailActivity);
        binding.rvPopularMovies.setLayoutManager(new GridLayoutManager(this, 2));
        binding.rvPopularMovies.setAdapter(adapter);
    }

    private boolean isConnectedToInternet(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }

}
