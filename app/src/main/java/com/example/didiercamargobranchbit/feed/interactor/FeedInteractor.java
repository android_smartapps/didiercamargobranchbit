package com.example.didiercamargobranchbit.feed.interactor;

import com.example.didiercamargobranchbit.feed.data.Movie;

import java.util.List;

public interface FeedInteractor {

    interface FeedListener{
        void onSuccessFeed(List<Movie> popularMovies);
        void onErrorFeed(String errorMessage);
        void onFailureFeed(String failureMessage);
    }

}
