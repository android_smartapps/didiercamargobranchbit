package com.example.didiercamargobranchbit.feed.interactor;

import com.example.didiercamargobranchbit.feed.data.Movie;

import java.util.List;

public interface FeedOfflineInteractor {

    interface GetFeedOfflineListener {
        void onSuccessGetFeedOffline(List<Movie> popularMovies);
        void onErrorGetFeedOffline(String errorMessage);
        void onFailureGetFeedOffline(String failureMessage);
    }

}
