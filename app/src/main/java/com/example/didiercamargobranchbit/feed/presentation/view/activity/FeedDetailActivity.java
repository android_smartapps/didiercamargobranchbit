package com.example.didiercamargobranchbit.feed.presentation.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.example.didiercamargobranchbit.R;
import com.example.didiercamargobranchbit.app.base.BaseActivity;
import com.example.didiercamargobranchbit.databinding.ActivityFeedDetailBinding;
import com.example.didiercamargobranchbit.feed.data.Movie;

public class FeedDetailActivity extends BaseActivity {

    private static final String URL_MOVIE_IMAGES = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";
    public static final String EXTRA_MOVIE = "FeedDetailActivity.movie";
    private Movie movie;
    private ActivityFeedDetailBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feed_detail);

        movie = getIntent().getParcelableExtra(EXTRA_MOVIE);
        binding.setMovie(movie);
        Glide.with(this)
                .load(String.format("%s%s", URL_MOVIE_IMAGES, movie.getBackdropPath()))
                .into(binding.ivPictureMovie);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
