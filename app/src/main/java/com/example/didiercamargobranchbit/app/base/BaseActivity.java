package com.example.didiercamargobranchbit.app.base;

import androidx.appcompat.app.AppCompatActivity;

import com.example.didiercamargobranchbit.R;
import com.example.didiercamargobranchbit.app.dialogfragment.MessageDialogFragment;

public class BaseActivity extends AppCompatActivity implements BaseView{

    @Override
    public void showErrorMessage() {
        MessageDialogFragment dialogFragment =
                MessageDialogFragment.newInstance(
                        getResources().getString(R.string.error_title),
                        getResources().getString(R.string.error_message),
                        R.mipmap.icon_error,
                        getResources().getColor(R.color.error));
        dialogFragment.setMessageAcceptListener(() -> {
            dialogFragment.dismiss();
        });
        dialogFragment.setCancelable(false);
        dialogFragment.setTitleSize(MessageDialogFragment.TITLE_SIZE_MEDIUM);
        dialogFragment.show(getSupportFragmentManager(), null);
    }
}
