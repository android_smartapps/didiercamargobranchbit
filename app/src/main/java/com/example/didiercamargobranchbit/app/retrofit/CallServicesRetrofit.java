package com.example.didiercamargobranchbit.app.retrofit;

import com.example.didiercamargobranchbit.feed.data.RequestResultPopularMovies;
import com.example.didiercamargobranchbit.feed.interactor.FeedInteractor;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallServicesRetrofit {

    private static final String TAG = CallServicesRetrofit.class.getSimpleName();
    private static SOService mService;

    public static void executeGetPopularMovies(FeedInteractor.FeedListener getPopularMovies){
        mService = ApiUtilsRetrofit.getSOService();
        mService.getPopularMovies().enqueue(new Callback<RequestResultPopularMovies>() {
            @Override
            public void onResponse(Call<RequestResultPopularMovies> call, Response<RequestResultPopularMovies> response) {
                getPopularMovies.onSuccessFeed(response.body().getResults());
            }

            @Override
            public void onFailure(Call<RequestResultPopularMovies> call, Throwable t) {

            }
        });
    }

}
