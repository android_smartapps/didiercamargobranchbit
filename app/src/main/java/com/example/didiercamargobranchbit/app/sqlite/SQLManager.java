package com.example.didiercamargobranchbit.app.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;

import com.example.didiercamargobranchbit.app.sqlite.MoviesDBContract.PopularMoviesEntry;
import com.example.didiercamargobranchbit.feed.data.Movie;
import com.example.didiercamargobranchbit.feed.interactor.FeedOfflineInteractor;
import com.example.didiercamargobranchbit.feed.interactor.SaveFeedInteractor;

import java.util.ArrayList;
import java.util.List;

public class SQLManager {

    private static final String SQL_CREATE_POPULAR_MOVIES =
            "CREATE TABLE " + PopularMoviesEntry.TABLE_NAME + " (" +
                    PopularMoviesEntry.COLUMN_ID + " TEXT PRIMARY KEY" + "," +
                    PopularMoviesEntry.COLUMN_BACKDROP_PATH + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_ADULT + " BOOLEAN" + "," +
                    PopularMoviesEntry.COLUMN_ORIGINAL_LANGUAGE + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_ORIGINAL_TITLE + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_OVER_VIEW + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_POPULARITY + " FLOAT" + "," +
                    PopularMoviesEntry.COLUMN_POSTHER_PATH + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_RELEASE_DATE + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_TITLE + " TEXT" + "," +
                    PopularMoviesEntry.COLUMN_VIDEO + " BOOLEAN" + "," +
                    PopularMoviesEntry.COLUMN_VOTE_AVERAGE + " FLOAT" + "," +
                    PopularMoviesEntry.COLUMN_VOTE_COUNT + " INT)";
    private static MoviesDBHelper mDBHelper;

    public SQLManager(Context context) {
        mDBHelper = new MoviesDBHelper(context);
    }

    public class MoviesDBHelper extends SQLiteOpenHelper {

        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "MoviesDB.db";

        public MoviesDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_POPULAR_MOVIES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    public static void saveMovie(final Movie movie, final SaveFeedInteractor.SaveFeedListener saveFeedListener) {
        new AsyncTask<Movie, Void, Void>(){
            @Override
            protected Void doInBackground(Movie... movies) {
                SQLiteDatabase db = mDBHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(PopularMoviesEntry.COLUMN_ID, movie.getId());
                values.put(PopularMoviesEntry.COLUMN_ADULT, movie.getAdult());
                values.put(PopularMoviesEntry.COLUMN_BACKDROP_PATH, movie.getBackdropPath());
                values.put(PopularMoviesEntry.COLUMN_ORIGINAL_LANGUAGE, movie.getOriginalLanguage());
                values.put(PopularMoviesEntry.COLUMN_ORIGINAL_TITLE, movie.getOriginalTitle());
                values.put(PopularMoviesEntry.COLUMN_OVER_VIEW, movie.getOverview());
                values.put(PopularMoviesEntry.COLUMN_POPULARITY, movie.getPopularity());
                values.put(PopularMoviesEntry.COLUMN_POSTHER_PATH, movie.getPosterPath());
                values.put(PopularMoviesEntry.COLUMN_RELEASE_DATE, movie.getReleaseDate());
                values.put(PopularMoviesEntry.COLUMN_TITLE, movie.getTitle());
                values.put(PopularMoviesEntry.COLUMN_VIDEO, movie.getVideo());
                values.put(PopularMoviesEntry.COLUMN_VOTE_AVERAGE, movie.getVoteAverage());
                values.put(PopularMoviesEntry.COLUMN_VOTE_COUNT, movie.getVoteCount());
                db.insert(PopularMoviesEntry.TABLE_NAME, "", values);
                return null;
            }

            @Override
            protected void onPostExecute(Void unused) {
                super.onPostExecute(unused);
                if(saveFeedListener != null){
                    saveFeedListener.onSuccessSaveFeed();
                }
            }
        }.execute();
    }

    public static void getPopularMovies(FeedOfflineInteractor.GetFeedOfflineListener getFeedOfflineListener) {
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        String[] projection = {
                PopularMoviesEntry.COLUMN_ID,
                PopularMoviesEntry.COLUMN_ADULT,
                PopularMoviesEntry.COLUMN_BACKDROP_PATH,
                PopularMoviesEntry.COLUMN_ORIGINAL_LANGUAGE,
                PopularMoviesEntry.COLUMN_ORIGINAL_TITLE,
                PopularMoviesEntry.COLUMN_OVER_VIEW,
                PopularMoviesEntry.COLUMN_POPULARITY,
                PopularMoviesEntry.COLUMN_POSTHER_PATH,
                PopularMoviesEntry.COLUMN_RELEASE_DATE,
                PopularMoviesEntry.COLUMN_TITLE,
                PopularMoviesEntry.COLUMN_VIDEO,
                PopularMoviesEntry.COLUMN_VOTE_AVERAGE,
                PopularMoviesEntry.COLUMN_VOTE_COUNT
        };
        Cursor cursor = db.query(
                PopularMoviesEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );
        List<Movie> list = new ArrayList<>();
        while(cursor.moveToNext()){
            Movie movie = new Movie();
            movie.setId(cursor.getInt(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_ID)));
            movie.setAdult(cursor.getInt(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_ADULT)) > 0);
            movie.setBackdropPath(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_BACKDROP_PATH)));
            movie.setOriginalLanguage(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_ORIGINAL_LANGUAGE)));
            movie.setOriginalTitle(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_ORIGINAL_TITLE)));
            movie.setOverview(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_OVER_VIEW)));
            movie.setPopularity(cursor.getDouble(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_POPULARITY)));
            movie.setPosterPath(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_POSTHER_PATH)));
            movie.setReleaseDate(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_RELEASE_DATE)));
            movie.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_TITLE)));
            movie.setVideo(cursor.getInt(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_VIDEO)) > 0);
            movie.setVoteAverage(cursor.getDouble(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_VOTE_AVERAGE)));
            movie.setVoteCount(cursor.getColumnIndexOrThrow(PopularMoviesEntry.COLUMN_VOTE_COUNT));
            list.add(movie);
        }
        cursor.close();
        getFeedOfflineListener.onSuccessGetFeedOffline(list);
    }
}
