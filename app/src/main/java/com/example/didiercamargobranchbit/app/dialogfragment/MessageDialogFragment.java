package com.example.didiercamargobranchbit.app.dialogfragment;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.didiercamargobranchbit.databinding.DialogFragmentMessageBinding;

public class MessageDialogFragment extends DialogFragment {

    public static final int TITLE_SIZE_SMALL = 0;
    public static final int TITLE_SIZE_MEDIUM = 1;
    public static final int TITLE_SIZE_BIG = 2;
    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String ICON = "icon";
    private static final String COLOR = "color";
    private String title;
    private String message;
    private int icon;
    private int color;
    private int titleSize;
    private Resources res;
    private DialogFragmentMessageBinding binding;
    private MessageAcceptListener messageAcceptListener;

    public interface MessageAcceptListener {
        void onClickAccept();
    }

    public MessageDialogFragment() {

    }

    public static MessageDialogFragment newInstance(String title, String message, int icon, int color) {
        MessageDialogFragment dialogFragment = new MessageDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(TITLE, title);
        bundle.putString(MESSAGE, message);
        bundle.putInt(ICON, icon);
        bundle.putInt(COLOR, color);
        dialogFragment.setArguments(bundle);
        return dialogFragment;
    }

    public void setMessageAcceptListener(MessageAcceptListener messageAcceptListener) {
        this.messageAcceptListener = messageAcceptListener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DialogFragmentMessageBinding.inflate(inflater, container, false);
        binding.setAcceptListener(new AcceptListener());
        final View view = binding.getRoot();

        res = getResources();

        Bundle bundle = getArguments();
        assert bundle != null;
        title = bundle.getString(TITLE);
        message = bundle.getString(MESSAGE);
        icon = bundle.getInt(ICON);
        color = bundle.getInt(COLOR);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        binding.tvTitle.setTextSize(titleSize);

        binding.ivTopLine.setBackgroundColor(color);
        binding.ivIcon.setImageResource(icon);
        binding.ivMediumLine.setBackgroundColor(color);
        binding.tvTitle.setTextColor(color);
        binding.tvTitle.setText(title);
        binding.tvMessage.setText(String.format("%s", message));
        binding.cvAccept.setCardBackgroundColor(color);
        binding.tvAccept.setTextColor(res.getColor(android.R.color.white));
        binding.ivBottomLine.setBackgroundColor(color);
    }

    public void setTitleSize(int size) {
        if (size == TITLE_SIZE_SMALL) {
            titleSize = 16;
        } else if (size == TITLE_SIZE_MEDIUM) {
            titleSize = 20;
        } else if (size == TITLE_SIZE_BIG) {
            titleSize = 24;
        }
    }

    public class AcceptListener {

        public void onClickAccept(View view) {
            messageAcceptListener.onClickAccept();
        }
    }

}
