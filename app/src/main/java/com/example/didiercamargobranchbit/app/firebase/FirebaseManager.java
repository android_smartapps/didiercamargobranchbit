package com.example.didiercamargobranchbit.app.firebase;

import android.net.Uri;
import android.util.Log;

import com.example.didiercamargobranchbit.gallery.interactor.SavePicturesFirebaseInteractor;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.List;

public class FirebaseManager {

    private static final String TAG = FirebaseManager.class.getSimpleName();
    private static FirebaseStorage storage;

    public FirebaseManager(){
        storage = FirebaseStorage.getInstance();
    }

    public static void savePictures(List<Uri> images,
                                    SavePicturesFirebaseInteractor.SavePicturesFirebaseListener
                                            savePicturesFirebaseListener){
        StorageReference storageRef = storage.getReference();
        for(Uri image : images) {
            StorageReference riversRef = storageRef.child("images/" + image.getLastPathSegment());
            UploadTask uploadTask = riversRef.putFile(image);

            uploadTask.addOnFailureListener(exception -> {
                Log.d(TAG, "Failure xd");
            }).addOnSuccessListener(taskSnapshot -> {
                savePicturesFirebaseListener.onSuccessSavePicturesFirebase();
            });
        }
    }

}
