package com.example.didiercamargobranchbit.app.retrofit;

import com.example.didiercamargobranchbit.feed.data.RequestResultPopularMovies;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SOService {

    @GET("movie/popular?api_key=739375c1ae35900d3a073d004fa764d1&language=es&page=1")
    Call<RequestResultPopularMovies> getPopularMovies();

}
