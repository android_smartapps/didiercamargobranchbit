package com.example.didiercamargobranchbit.app.retrofit;

public class ApiUtilsRetrofit {

    private static final String BASE_URL_DBMOVIE = "https://api.themoviedb.org/3/";

    public static SOService getSOService() {
        return RetrofitClient.getClient(BASE_URL_DBMOVIE).create(SOService.class);
    }

}
