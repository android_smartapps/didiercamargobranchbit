package com.example.didiercamargobranchbit.app.sqlite;

public class MoviesDBContract {

    public MoviesDBContract(){

    }

    public static class PopularMoviesEntry{
        static final String TABLE_NAME = "PopularMovies";
        static final String COLUMN_ID = "id";
        static final String COLUMN_ADULT = "adult";
        static final String COLUMN_BACKDROP_PATH ="backdropPath";
        static final String COLUMN_ORIGINAL_LANGUAGE = "originalLanguage";
        static final String COLUMN_ORIGINAL_TITLE = "originalTitle";
        static final String COLUMN_OVER_VIEW = "overview";
        static final String COLUMN_POPULARITY = "popularity";
        static final String COLUMN_POSTHER_PATH = "postherPath";
        static final String COLUMN_RELEASE_DATE = "releaseDate";
        static final String COLUMN_TITLE = "title";
        static final String COLUMN_VIDEO = "video";
        static final String COLUMN_VOTE_AVERAGE = "voteAverage";
        static final String COLUMN_VOTE_COUNT = "voteCount";
    }

}
