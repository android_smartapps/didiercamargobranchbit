package com.example.didiercamargobranchbit;

import android.app.Application;

import com.example.didiercamargobranchbit.app.firebase.FirebaseManager;
import com.example.didiercamargobranchbit.app.sqlite.SQLManager;

public class DidierCamargoBranchbit extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new SQLManager(getApplicationContext());
        new FirebaseManager();
    }
}
